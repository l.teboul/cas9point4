include { UNTAR as UNTAR_FAST5 } from '../../modules/nf-core/untar/main.nf'
include { POD5_CONVERT         } from '../../modules/local/pod5/convert/main'
include { DORADO_BASECALLER    } from '../../modules/local/dorado/basecaller/main'
include { DORADO_DEMUX         } from '../../modules/local/dorado/demux/main'

workflow DEMULTIPLEX_FAST5 {
    take:
    ch_data       // Parsed samplesheet data
    fast5         // fast5 folder
    dorado_model  // val
    dorado_bc_kit // val
    pod5_mode     // val

    main:
    ch_versions = Channel.empty()

    //
    // MODULE: Uncompress fast5 file if required
    //
    if (fast5.endsWith(".tar.gz")) {
        ch_fast5    = UNTAR_FAST5 ( [ [], file(fast5) ] ).untar.map{ row -> [ [id:"fast5"], row[1] ] }
        ch_versions = ch_versions.mix(UNTAR_FAST5.out.versions)
    } else {
        ch_fast5 = Channel.from( file(fast5) ).map { row -> [[id:"fast5"], row] }
    }

    //
    // MODULE: Convert fast5 to pod5
    //
    if(pod5_mode) {
        ch_pod5 = Channel.fromPath( "${fast5}/*" ).collect().map { row -> [[id:"pod5"], row] }
    }
    else {
        POD5_CONVERT (
            ch_fast5
        )
        ch_versions = ch_versions.mix(POD5_CONVERT.out.versions)
        ch_pod5 = POD5_CONVERT.out.pod5.map { row -> [[id:"pod5"], row[1]] }
    }

    //
    // MODULE: Basecall pod5 and produce either bam or fastq
    //
    DORADO_BASECALLER (
        ch_pod5,
        dorado_model
    )
    ch_versions = ch_versions.mix(DORADO_BASECALLER.out.versions)
    ch_fastq    = DORADO_BASECALLER.out.fastq
    ch_bam      = DORADO_BASECALLER.out.bam

    //
    // MODULE: demux if required
    //
    if(dorado_bc_kit) {
        DORADO_DEMUX (
            ch_bam
        )
        ch_versions = ch_versions.mix(DORADO_DEMUX.out.versions)
        ch_fastq    = DORADO_DEMUX.out.fastq
    }

    //
    // CHANNEL: Set the metadata to the barcode for the fastq files
    //
    ch_fastq_meta = ch_fastq
        .map { it[1] }
        .flatten()
        .map{ it ->
            def file_name = it.baseName.toString()
            def barcode = "unclassified"
            int index = file_name.indexOf("barcode")
            if(index != -1) {
                barcode = file_name.substring(index, index + "barcode".length() + 2)
            }
            [ barcode, it ]
        }

    //
    // CHANNEL: Merges the fastq files with the main data channel
    //
    ch_data_fastq = ch_data
        .map{ [it[0].barcode, it] }
        .combine(ch_fastq_meta)
        .filter { it[0] == it[2] }
        .map { [ it[1][0], it[3], it[1][2], it[1][3] ] }

    //
    // CHANNEL: Extract reads into seperate channel
    //
    ch_reads = ch_data_fastq.map{[it[0], it[1]]}

    emit:
    data     = ch_data_fastq // channel: [ val(meta), fastq, ref, [dets] ]
    reads    = ch_reads      // channel: [ val(meta), fastq ]
    versions = ch_versions   // channel: [ versions.yml ]
}
