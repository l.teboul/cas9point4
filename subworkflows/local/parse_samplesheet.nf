import java.nio.file.Paths

include { CHECK_SAMPLESHEET           } from '../../modules/local/python/check_samplesheet/main'
include { LINUX_COMMAND as GZIP_FASTQ } from '../../modules/local/linux/command/main'

workflow PARSE_SAMPLESHEET {
    take:
    samplesheet // file: /path/to/samplesheet.csv

    main:
    ch_versions = Channel.empty()

    //
    // MODULE: Check the samplesheet for errors
    //
    CHECK_SAMPLESHEET (
        samplesheet
    )
    ch_versions = CHECK_SAMPLESHEET.out.versions

    //
    // CHANNEL: Parse samplesheet into metadata
    //
    ch_data = CHECK_SAMPLESHEET.out.csv
        .splitCsv ( header:true, sep:"," )
        .map { parse_meta(it) }

    //
    // CHANNEL: Create output channels
    //
    ch_reads = ch_data.map{[it[0], it[1]]}
    ch_roi   = ch_data.map{[it[0], it[2]]}
    ch_dets  = ch_data.map{[it[0], it[3]]}

    //
    // CHANNEL: Get uncompressed fastq files
    //
    ch_reads_uncom = ch_reads.filter { row ->
            row[1].toString().endsWith(".fastq")
    }

    //
    // MODULE: Zip the files if needed
    //
    GZIP_FASTQ (
        ch_reads_uncom,
        [],
        false
    )

    // CHANNEL: merge the files back to the compressed ones
    ch_reads = ch_reads
        .filter { row -> !(row[1].toString().endsWith(".fastq")) }
        .mix(GZIP_FASTQ.out.file)

    emit:
    data     = ch_data     // channel: [ val(meta), path ]
    reads    = ch_reads    // channel: [ val(meta), path ]
    roi      = ch_roi      // channel: [ val(meta), path ]
    dets     = ch_dets     // channel: [ val(meta), path ]
    versions = ch_versions // channel: [ versions.yml ]
}

def parse_meta(LinkedHashMap row) {
    // Setup files
    def dets = []
    def reads = []
    roi_ref = file(row['roi_ref'], checkIfExists: true)
    if(row['reads'] != "") {
        reads = file(row['reads'], checkIfExists: true)
    }
    det1 = file(row['determinant_1'], checkIfExists: true)
    dets.add(det1)
    if(row['determinant_2'] != "") {
        det2 = file(row['determinant_2'], checkIfExists: true)
        dets.add(det2)
    }
    if(row['determinant_3'] != "") {
        det3 = file(row['determinant_3'], checkIfExists: true)
        dets.add(det3)
    }

    // Create metadata
    def meta = [:]
    roi_name = roi_ref.getName().split('\\.')[0].toLowerCase()
    meta.id = row['id']
    meta.sample_name = row['sample_name']
    meta.barcode = row['bc_name']
    meta.roi_name = roi_name
    meta.length_perc = row['length_perc']

    // Check if we have a fast5 folder and kit name
    if(reads.size() == 0 && params.fast5 == null) {
        exit 1, "No fast5 folder specified"
    }

    // Output final structure
    array = [ meta, reads, roi_ref, dets ]
    return array
}
