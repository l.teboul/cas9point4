process CLAIR3_CALL {
    tag "$meta.id"
    label 'process_high'

    container "docker.io/hkubal/clair3:v1.0.4"

    input:
    tuple val(meta), path(bam), path(bai), path(ref), path(fai)
    val(model)
    path(model_file)

    output:
    tuple val(meta), path("*.vcf"), emit: vcf
    path "versions.yml"           , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    def model_path = model_file ? "./${model_file}" : "/opt/models/${model}"
    """
    /opt/bin/run_clair3.sh \\
        --platform="ont" \\
        --output=. \\
        --threads=$task.cpus \\
        --bam_fn=${bam} \\
        --ref_fn=${ref} \\
        --model_path=${model_path} \\
        --sample_name=${meta.id} \\
        $args \\

    gzip -dc merge_output.vcf.gz > ${prefix}.vcf

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        clair3: \$(echo \$(/opt/bin/run_clair3.sh --version 2>&1) | sed 's/^.*Clair3 //; s/ .*\$//')
    END_VERSIONS
    """
}
