process POD5_CONVERT {
    tag "$meta.id"
    label 'process_med_cpu'

    container "docker.io/goodwright/cas9point4_pod5:latest"

    input:
    tuple val(meta), path(fast5)

    output:
    tuple val(meta), path("pod5/*.pod5"), emit: pod5
    path  "versions.yml"                , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    """
    pod5 convert fast5 \\
        --threads $task.cpus \\
        $args \\
        ./${fast5}/*.fast5 \\
        --output pod5/ \\
        --one-to-one ./${fast5}/

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        pod5: \$(pod5 --version | grep -oP '(?<=version: )\\S+')
    END_VERSIONS
    """
}
