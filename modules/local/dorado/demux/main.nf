process DORADO_DEMUX {
    tag "$meta.id"
    label "process_single"

    container "docker.io/goodwright/cas9point4_dorado:latest"

    input:
    tuple val(meta), path(bam)

    output:
    tuple val(meta), path("*.gz") , emit: fastq
    path  "versions.yml"          , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args   = task.ext.args ?: ""
    """
    dorado demux \\
        --output-dir . \\
        $args \\
        $bam

    for file in ./*.fastq; do
        gzip \"\$file\"
    done

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        dorado: \$(dorado --version 2>&1)
    END_VERSIONS
    """
}
