process DORADO_BASECALLER {
    tag "$meta.id"
    label "process_high"

    container "docker.io/goodwright/cas9point4_dorado:latest"

    input:
    tuple val(meta), path("pod5s/*")
    val model

    output:
    tuple val(meta), path("*.gz") , emit: fastq, optional: true
    tuple val(meta), path("*.bam"), emit: bam, optional: true
    path  "versions.yml"          , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args   = task.ext.args ?: ""
    def prefix = task.ext.prefix ?: "${meta.id}"
    def bc_kit = task.ext.bc_kit ?: ""
    def output = task.ext.bc_kit == "" ? " | gzip > unclassified.fastq.gz" : " > ${prefix}.bam"
    """
    dorado basecaller \\
        $model \\
        pod5s/ \\
        $bc_kit \\
        $args \\
        $output

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        dorado: \$(dorado --version 2>&1)
    END_VERSIONS
    """
}
