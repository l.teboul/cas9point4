process PYSAMSTATS {
    tag "$meta.id"
    label 'process_low'

    conda "bioconda::pysamstats=0.24.2"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/pysamstats:0.24.2--py35_1' :
        'biocontainers/pysamstats:0.24.2--py35_1' }"

    input:
    tuple val(meta), path(bam), path(bai)
    path(fasta)

    output:
    tuple val(meta), path("*.pysam"), emit: pysam
    path  "versions.yml"            , emit: versions

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    def prefix = task.ext.prefix ?: "${meta.id}"
    def ref = fasta ? "-f ${fasta}" : ""
    """
    pysamstats \\
        $args \\
        $ref \\
        $bam \\
        > ${prefix}.pysam

    cat <<-END_VERSIONS > versions.yml
    "${task.process}":
        pysamstats: \$(pysamstats --help | grep 'Version:' | tail -n 1 | sed 's/Version: \\([0-9]*\\.[0-9]*\\.[0-9]*\\).*/\\1/')
    END_VERSIONS
    """
}
