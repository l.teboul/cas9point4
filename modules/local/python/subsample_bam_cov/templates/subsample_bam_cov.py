#!/usr/bin/env python3

import platform
from argparse import ArgumentParser
import pysam
import pandas as pd


def dump_versions(process_name):
    """
    Writes the versions of python and libraries to a YAML file.

    Args:
    process_name (str): Name of the process to be logged in the file.
    """
    with open("versions.yml", "w", encoding="utf-8") as out_f:
        out_f.write(process_name + ":\n")
        out_f.write("    python: " + platform.python_version() + "\n")
        out_f.write("    pysam: " + pysam.__version__ + "\n")
        out_f.write("    pandas: " + pd.__version__ + "\n")


def subsample_bam(in_bam_path, out_bam_path, cov_path, cov_thresh):
    """
    Filters a BAM file based on a coverage threshold and writes the output to a new file.

    This function calculates the average coverage of a BAM file and, if it exceeds
    a given threshold, subsamples the file to reduce the coverage. The subsampled
    BAM file is then saved to the specified output path.

    Args:
        in_bam_path (str): Path to the input BAM file.
        out_bam_path (str): Path where the output BAM file will be saved.
        cov_path (str): Path to the coverage file used to calculate average coverage.
        cov_thresh (float): The coverage threshold for subsampling.

    Side Effects:
        Writes a subsampled BAM file to 'out_bam_path' and creates its index.
    """

    # Open files
    out_bam = open(out_bam_path, 'wb')
    cov_df = pd.read_csv(cov_path, sep='\t', header=None)

    # Calculate the average coverage for the sample
    av_cov = sum(cov_df[2]) / max(cov_df[1])
    print(f'Average coverage: {av_cov}')

    # Exit if we have no reads
    if av_cov < 0.1:
        print("No coverage, exiting")
        return

    # Calculate if the av cov is above or below the ratio threshold
    cov_ratio = cov_thresh / av_cov
    print(f'Coverage ratio: {cov_ratio}')

    # If we are above then there is no need to filter the file
    cov_ratio = min(cov_ratio, 0.999999999)

    # Randomly subsample bam file and then index
    pysam_op = pysam.view("-s", str(cov_ratio), "-b", in_bam_path)
    out_bam.write(pysam_op)
    out_bam.close()
    pysam.index(out_bam_path)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-i', '--input', default="!{bam_input}")
    parser.add_argument('-o', '--output', default="!{bam_output}")
    parser.add_argument('-c', '--coverage', default="!{cov_input}")
    parser.add_argument('-t', '--coverage_thresh', default="!{coverage_thresh}")
    parser.add_argument('-p', '--process_name', default="!{process_name}")
    opts, unknown_args = parser.parse_known_args()

    print('Subsample BAM Cov')
    subsample_bam(opts.input, opts.output, opts.coverage, float(opts.coverage_thresh))
    dump_versions(opts.process_name)
