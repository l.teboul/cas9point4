process SUBSAMPLE_BAM_COV {
    tag "${meta.id}"
    label "process_single"

    container "docker.io/goodwright/bamtools:latest"

    input:
    tuple val(meta), path(bam), path(coverage)
    val(coverage_thresh)

    output:
    tuple val(meta), path("*.bam"), emit: bam, optional: true
    tuple val(meta), path("*.bai"), emit: bai, optional: true
    path  "versions.yml"          , emit: versions

    when:
    task.ext.when == null || task.ext.when

    shell:
    def prefix = task.ext.prefix ?: "${meta.id}"

    process_name = task.process
    bam_input    = bam
    bam_output   = "${prefix}.bam"
    cov_input    = coverage

    template 'subsample_bam_cov.py'
}
