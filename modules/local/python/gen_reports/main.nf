process GEN_REPORTS {
    label "process_single"

    container "docker.io/goodwright/cas9point4_reporting:latest"

    input:
    path ('coverage/*')
    path ('strand_var/*')

    output:
    path("*.png")        , emit: png
    path("*.pdf")        , emit: pdf
    path  "versions.yml" , emit: versions

    when:
    task.ext.when == null || task.ext.when

    shell:
    arg_process_name = task.process
    arg_output       = '.'
    arg_coverage     = './coverage/*.*'
    arg_strand_var   = './strand_var/*.*'

    template 'gen_reports.py'
}
