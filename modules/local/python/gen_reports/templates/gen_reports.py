#!/usr/bin/env python3

import platform
import os
import glob
from argparse import ArgumentParser

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages

# Init
sns.set(style="whitegrid")


def dump_versions(process_name):
    """
    Writes the versions of python and libraries to a YAML file.

    Args:
    process_name (str): Name of the process to be logged in the file.
    """
    with open("versions.yml", "w", encoding="utf-8") as out_f:
        out_f.write(process_name + ":\n")
        out_f.write("    python: " + platform.python_version() + "\n")
        out_f.write("    seaborn: " + sns.__version__ + "\n")
        out_f.write("    pandas: " + pd.__version__ + "\n")


def coverage_reports(output_path, coverage_path):
    print("\nGenerating coverage reports")

    # Grab files
    file_list = glob.glob(coverage_path)
    file_list.sort()

    # Create dataframe
    dataframes = []
    for file_path in file_list:
        df = pd.read_csv(file_path, sep='\t', names=['ref', 'pos', 'depth'])
        sample_name = os.path.splitext(os.path.basename(file_path))[0]
        df['sample_id'] = sample_name
        dataframes.append(df)
    coverage_df = pd.concat(dataframes, ignore_index=True)

    # Output data
    coverage_df.to_csv(os.path.join(output_path, 'coverage_data.tsv'), sep='\t', index=False)

    # Create plots and pdfs
    with PdfPages(os.path.join(output_path, 'coverage_plots.pdf')) as pdf:
        # Creating and saving each plot
        for idx, sample_id in enumerate(coverage_df['sample_id'].unique()):
            fig, ax = plt.subplots(figsize=(12, 4))

            # Subset data for the current sample_id
            subset = coverage_df[coverage_df['sample_id'] == sample_id]

            # Plotting with Seaborn
            sns.lineplot(x='pos', y='depth', data=subset, ax=ax)
            ax.set_xlim(subset['pos'].min(), subset['pos'].max())
            ax.set_ylabel('Depth')
            ax.set_xlabel('Position')
            ax.set_title(f'Read Coverage: {sample_id}')

            # Save the plot as a PNG file
            plot_filename = os.path.join(output_path, f'{sample_id}_coverage.png')
            plt.savefig(plot_filename, bbox_inches='tight')

            # Add the plot to the PDF
            pdf.savefig(fig, bbox_inches='tight')

            # Close the plot to free memory
            plt.close(fig)


def accuracy(r, base):
    if r['ref'] == r[base]:
        return 101
    else:
        return 0


def strand_variation_reports(output_path, strandvar_path):
    print("\nGenerating strand variation reports")

    # Init
    bases = ['A', 'T', 'C', 'G', 'insertions', 'deletions']

    # Grab files
    file_list = glob.glob(strandvar_path)
    file_list.sort()

    # Create plots and PDF's
    with PdfPages(os.path.join(output_path, 'strand_variation_plots.pdf')) as pdf:
        for file_path in file_list:
            # Get sample name
            sample_name = os.path.splitext(os.path.basename(file_path))[0]

            # Read report data
            df = pd.read_csv(file_path, sep='\t')

            # Scale data to percent
            for b in bases:
                df[f'{b}_percent'] = (df[b] / df['reads_all']) * 100

            # Calc extra metrics
            df['reads_all'] = df[['A', 'T', 'C', 'G', 'deletions', 'insertions']].sum(axis=1)
            df['top_base'] = df[['A', 'T', 'C', 'G']].max(axis=1)
            df['top_base_seq'] = df[['A', 'T', 'C', 'G']].idxmax(axis=1)
            df['1st'] = (df['top_base'] / df['reads_all']) * 100
            df['top_base_accuracy'] = df.apply(accuracy, axis=1, args=('top_base_seq',))

            # More summary metrics
            df2 = df[['pos', '1st', 'insertions_percent', 'deletions_percent', 'top_base_accuracy']]
            df2 = pd.melt(df2, id_vars=['pos'], value_vars=['1st', 'insertions_percent', 'deletions_percent', 'top_base_accuracy'])
            df2 = df2.rename(columns={'variable': 'base majority', 'value': 'Percent of base in pileup'})

            # Create plot
            fig, ax = plt.subplots(figsize=(12, 4))
            sns.lineplot(x='pos', y='Percent of base in pileup', data=df2, ax=ax)
            ax.set_xlabel('Position')
            ax.set_title(f'Strand Variation: {sample_name}')
            plt.savefig(os.path.join(output_path, f'{sample_name}_coverage.png'), bbox_inches='tight')

            # Save to PDF as well
            pdf.savefig(fig, bbox_inches='tight')

            # Cleanup
            plt.close(fig)


if __name__ == '__main__':
    # Parse Args
    parser = ArgumentParser()
    parser.add_argument('-p', '--process_name', default="!{arg_process_name}")
    parser.add_argument('-o', '--output', default="!{arg_output}")
    parser.add_argument('-c', '--coverage', default="!{arg_coverage}")
    parser.add_argument('-s', '--strandvar', default="!{arg_strand_var}")

    opts, unknown_args = parser.parse_known_args()

    # Run
    coverage_reports(opts.output, opts.coverage)
    #strand_variation_reports(opts.output, opts.strandvar)
    dump_versions(opts.process_name)
