#!/usr/bin/env python3

# pylint: disable=no-member

import platform
from argparse import ArgumentParser
import os
import warnings

import pandas as pd
import pysam
# import pysamstats

# Init
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# Blast header names
BLAST_HEADER = [
    'query',
    'subject',
    'id',
    'alignment length',
    'mismatches',
    'gap openings',
    'query start',
    'query end',
    'subject start',
    'subject end',
    'E value',
    'bit score'
]


def dump_versions(process_name):
    """
    Writes the versions of python and libraries to a YAML file.

    Args:
    process_name (str): Name of the process to be logged in the file.
    """
    with open("versions.yml", "w", encoding="utf-8") as out_f:
        out_f.write(process_name + ":\n")
        out_f.write("    python: " + platform.python_version() + "\n")
        out_f.write("    pysam: " + pysam.__version__ + "\n")
        out_f.write("    pandas: " + pd.__version__ + "\n")


def round_to_nearest_multiple(x, n=50):
    return round(x / n) * n


def run(prefix, blast_path, dets_str, det_length_str, length_perc, bam, out_dir):
    # Split out inputs
    dets = dets_str.split(',')
    det_lengths = [int(i) for i in det_length_str.split(',')]

    # Run subprocesses
    bins = bin_reads(blast_path, dets, det_lengths, length_perc)
    process_bins(bins, bam, prefix, out_dir)


def bin_reads(blast_path, dets, det_lengths, length_perc):
    """
    Take read blast file for determinants against reads and group by hit numbers and types
    """
    print('Binning reads')

    # Init
    num_determinants = len(dets)

    # Read blast file
    blast_df = pd.read_csv(blast_path, sep='\t', names=BLAST_HEADER)
    total_blast = blast_df.shape[0]

    # Convert subject to lowercase for better matching
    blast_df['subject'] = blast_df['subject'].str.lower()

    # Filter blast for the target determinant names
    # (this is because the static determinant file may contain many sequences to recognize)
    blast_df_subset = blast_df[blast_df['subject'].isin(dets)]
    relevant_blast = blast_df_subset.shape[0]
    print(f"Found {relevant_blast} out of {total_blast} relevant blast hits")

    # Progressively filter the blast subset for only those alignments that are
    # within the length boundaries of all determinants
    for i in range(0, num_determinants):
        # Calculate the cutoff
        cutoff = (det_lengths[i]/100) * length_perc

        # Find alignments greater than the cutoff
        blast_df_subset = blast_df_subset[blast_df_subset['alignment length'] > cutoff]
    relevant_blast = blast_df_subset.shape[0]
    print(f"Found {relevant_blast:,} out of {total_blast:,} blast hits after alignment length filter")

    # This code creates a binned alignment start point (50) then
    # groups the DataFrame df by the columns 'query' and 'fqstart'.
    # Then, for each group, it finds the maximum 'bit score'.
    blast_df_subset['fqstart'] = blast_df_subset['query start'].map(round_to_nearest_multiple)
    idx = blast_df_subset.groupby(['query', 'fqstart'])['bit score'].transform(max) == blast_df_subset['bit score']
    blast_df_subset = blast_df_subset[idx]
    relevant_blast = blast_df_subset.shape[0]
    print(f"Found {relevant_blast:,} out of {total_blast:,} blast hits after binning")

    # Count group of query and group of query,subject
    blast_df_subset['determinants'] = blast_df_subset.groupby('query')['query'].transform('count')
    blast_df_subset['determinants count'] = blast_df_subset.groupby(['query', 'subject'])['subject'].transform('count')

    blast_df_subset.to_csv('data.csv', index=False)

    # Filter queries that only have the correct number of total determinants and only one of each
    blast_df_pass = blast_df_subset[blast_df_subset.determinants == num_determinants]
    blast_df_pass = blast_df_pass[blast_df_pass['determinants count'] == 1]
    relevant_blast = blast_df_pass.shape[0]
    print(f"Found {relevant_blast:,} out of {total_blast:,} blast hits after determinant filtering")

    # Summarize data
    passed = blast_df_pass['query'].tolist()
    failed = blast_df[~blast_df['query'].isin(passed)]
    failed = failed['query'].tolist()
    d = {'passed': passed, 'failed': failed}
    print(f"Total passed: {len(passed):,} - Total failed: {len(failed):,}")

    return d


def process_bins(bins, bam, prefix, out_dir):
    """
    Filter bam file against passed blast bins
    """
    print('\nProcessing bins')

    # Cycle through passed/failed
    for b in bins:
        # Init
        bam_out_path = os.path.join(out_dir, f"{prefix}.{b}.bam")

        # Filter the reads for passed/failed
        print(f"Filtering BAM for {b} reads")
        num_reads, num_filtered = filter_bam(bam_out_path, bins[b], bam)
        print(f"Filtered {num_filtered} of {num_reads} {b} reads")


def filter_bam(out_path, queries, bam, include=True):
    """
    Filter input bam file for either inclusive or exclusive queries in the supplied list
    Index bam and return the total queries and total match count
    """

    # Init
    in_bam = pysam.AlignmentFile(bam, "rb")
    out_bam = pysam.AlignmentFile(out_path, "wb", template=in_bam)
    n = 0

    # Filter loop
    for read in in_bam.fetch():
        if include is True:
            if read.query_name in queries:
                n += 1
                out_bam.write(read)
        else:
            if read.query_name not in queries:
                n += 1
                out_bam.write(read)
    out_bam.close()

    # Index the bam file
    print("Indexing...")
    pysam.index(out_path)

    return len(queries), n


if __name__ == '__main__':
    # Parse Args
    parser = ArgumentParser()
    parser.add_argument('-p', '--process_name', default="!{process_name}")
    parser.add_argument('-pr', '--prefix', default="!{arg_prefix}")
    parser.add_argument('-bl', '--blast', default="!{arg_blast}", help='blast file of the determinant against reads')
    parser.add_argument('-d', '--determinants', default="!{arg_dets}", help='A comma separated list of determinant names')
    parser.add_argument('-l', '--lengths', default="!{arg_det_length}", help='A comma separated list of determinant lengths')
    parser.add_argument('-lp', '--length_perc', default="!{arg_length_perc}", help='The minimum length of determinant overlap')
    parser.add_argument('-b', '--bam', default="!{arg_bam}", help='sorted and indexed bam file of reads against ref file')
    parser.add_argument('-o', '--output', default=".")
    opts, unknown_args = parser.parse_known_args()

    print('Filter BAM using BLAST')

    # Run program
    run(opts.prefix, opts.blast, opts.determinants, opts.lengths, int(opts.length_perc), opts.bam, opts.output)
    dump_versions(opts.process_name)



#     def __get_pysamstats(self, bam_path, ref):
#         bam = pysam.AlignmentFile(bam_path)
#         for rec in pysamstats.stat_variation_strand(bam, fafile=ref):
#             yield rec

#     def get_pysamstats(self, bam, ref):
#         ps = self.__get_pysamstats(bam, ref)
#         df = pd.DataFrame(ps)
#         return df

#     def __loxp_read_orientation(self, r):
#         if r['subject start'] < r['subject end']:
#             return 'F'
#         return 'R'

#     def load_blast(self, blast_path):
#         df = pd.read_csv(blast_path, sep='\t', names=BLAST_HEADER)
#         df['LoxP read orientation'] = df.apply(self.__loxp_read_orientation, axis=1)
#         return df

#     def load_bam_data(self, bam_path):
#         bam = pysam.AlignmentFile(bam_path, "rb")
#         for read in bam.fetch():
#             data = {'query': read.query_name,
#                     'flag': read.flag,
#                     'ref': read.reference_name,
#                     'pos': read.reference_start,
#                     'mapQ': read.mapping_quality}
#             yield data

#     def __lox_ref_orient(self, r):
#         if r['flag'] == 0:
#             if r['LoxP read orientation'] == 'F':
#                 return 'F'
#             elif r['LoxP read orientation'] == 'R':
#                 return 'R'
#         elif r['flag'] == 16:
#             if r['LoxP read orientation'] == 'F':
#                 return 'R'
#             elif r['LoxP read orientation'] == 'R':
#                 return 'F'

#     def __hit_start(self, r):
#         if r['LoxP ref orientation'] == 'F':
#             return r['pos'] + r['query start']
#         if r['LoxP ref orientation'] == 'R':
#             return r['pos'] + r['query end']

#     def __hit_end(self, r):
#         if r['LoxP ref orientation'] == 'F':
#             return r['pos'] + r['query end']
#         if r['LoxP ref orientation'] == 'R':
#             return r['pos'] + r['query start']

#     def merge_bam_blast(self, blast_df, bam_df):
#         # Merge and calculate extra columns
#         merged_df = bam_df.merge(blast_df, how='outer')
#         merged_df['LoxP ref orientation'] = merged_df.apply(self.__lox_ref_orient, axis=1)
#         merged_df['Hit ref start'] = merged_df.apply(self.__hit_start, axis=1)
#         merged_df['Hit ref end'] = merged_df.apply(self.__hit_end, axis=1)

#         # Group and return
#         grouped_merged_df = merged_df[['subject', 'query', 'LoxP ref orientation']].groupby(['subject','LoxP ref orientation'])
#         return grouped_merged_df

    # def plot(self, title, pysam_stats_df, merged_bam_blast_df, ref_blast_df):