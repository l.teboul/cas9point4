process FILTER_BAM_BLAST {
    tag "${meta.id}"
    label "process_single"

    container "docker.io/goodwright/bamtools:latest"

    input:
    tuple val(meta), path(bam), path(bai), path(blast)
    val dets
    val det_lengths

    output:
    tuple val(meta), path("*passed.bam"), emit: passed_bam
    tuple val(meta), path("*failed.bam"), emit: failed_bam
    path  "versions.yml"                , emit: versions

    when:
    task.ext.when == null || task.ext.when

    shell:

    def prefix = task.ext.prefix ?: "${meta.id}"

    process_name    = task.process
    arg_prefix      = prefix
    arg_blast       = blast
    arg_bam         = bam
    arg_dets        = dets
    arg_det_length  = det_lengths
    arg_length_perc = meta.length_perc

    template 'filter_bam_blast.py'
}
