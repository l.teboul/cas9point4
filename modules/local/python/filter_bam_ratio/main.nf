process FILTER_BAM_RATIO {
    tag "${meta.id}"
    label "process_single"

    container "docker.io/goodwright/bamtools:latest"

    input:
    tuple val(meta), path(bam), path(bai)
    val(align_ratio_thresh)
    val(mismatch_ratio_thresh)

    output:
    tuple val(meta), path("*.bam"), emit: bam
    path  "versions.yml"          , emit: versions

    when:
    task.ext.when == null || task.ext.when

    shell:
    def prefix = task.ext.prefix ?: "${meta.id}"

    process_name = task.process
    bam_input    = bam
    bam_output   = "${prefix}.bam"

    template 'filter_bam_ratio.py'
}
