#!/usr/bin/env python3
"""
BAM File Filter and Version Logger

This script provides functionality to filter a BAM (Binary Alignment Map)
file based on specific criteria such as alignment ratio and mismatch ratio.
It filters out reads that do not meet the specified thresholds. Additionally, it logs the versions of Python and the pysam library being used.

Features:
- Filters BAM files based on alignment ratio and mismatch ratio thresholds.
- Writes the output to a new BAM file.
- Logs the Python and pysam version information into a YAML file.

Usage:
    python bam_filter.py -i <input_bam> -o <output_bam> -a <alignment_ratio> -m <mismatch_ratio> -p <process_name>

Arguments:
- '-i' or '--input': Path to the input BAM file.
- '-o' or '--output': Path for the output filtered BAM file.
- '-a' or '--align_ratio_thresh': Alignment ratio threshold for filtering.
- '-m' or '--mismatch_ratio_thresh': Mismatch ratio threshold for filtering.
- '-p' or '--process_name': Name of the process for version logging.

Dependencies:
- pysam: A Python module for reading, manipulating, and writing genomic data sets in SAM/BAM format.
"""

import platform
from argparse import ArgumentParser
import pysam


def dump_versions(process_name):
    """
    Writes the versions of python and libraries to a YAML file.

    Args:
    process_name (str): Name of the process to be logged in the file.
    """
    with open("versions.yml", "w", encoding="utf-8") as out_f:
        out_f.write(process_name + ":\n")
        out_f.write("    python: " + platform.python_version() + "\n")
        out_f.write("    pysam: " + pysam.__version__ + "\n")


def filter_bam(in_bam, out_bam, align_ratio, mismatch_ratio):
    """
    Filters a BAM file based on alignment ratio and mismatch ratio.

    Args:
    in_bam (str): Path to the input BAM file.
    out_bam (str): Path for the output filtered BAM file.
    align_ratio (float): Threshold for alignment ratio.
    mismatch_ratio (float): Threshold for mismatch ratio.
    """
    samfile = pysam.AlignmentFile(in_bam, "rb")  # pylint: disable=no-member
    filtered_bam = pysam.AlignmentFile(out_bam, "wb", template=samfile)  # pylint: disable=no-member

    total_reads = 0
    passed_reads = 0
    for read in samfile.fetch():
        total_reads += 1
        alignment_length_ratio = read.query_alignment_length / read.query_length
        nm = read.get_tag('NM')
        nm_ratio = nm / float(read.query_alignment_length)

        if nm_ratio > mismatch_ratio or alignment_length_ratio < align_ratio:
            continue
        if read.is_secondary or read.is_supplementary:
            continue

        passed_reads += 1
        filtered_bam.write(read)

    filtered_bam.close()
    samfile.close()
    print(f"Passed {passed_reads} of {total_reads} reads")


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-i', '--input', default="!{bam_input}")
    parser.add_argument('-o', '--output', default="!{bam_output}")
    parser.add_argument('-a', '--align_ratio_thresh', default="!{align_ratio_thresh}")
    parser.add_argument('-m', '--mismatch_ratio_thresh', default="!{mismatch_ratio_thresh}")
    parser.add_argument('-p', '--process_name', default="!{process_name}")
    opts, unknown_args = parser.parse_known_args()

    print('Filter BAM')
    filter_bam(opts.input, opts.output, float(opts.align_ratio_thresh), float(opts.mismatch_ratio_thresh))
    dump_versions(opts.process_name)
