#!/usr/bin/env python

import os
import sys
import errno
import argparse
import platform


def make_dir(path):
    """
    Create a directory at the specified path.

    This function attempts to create a directory at the given path.
    If the directory already exists, the function will not create a new one or raise an exception.
    If any other `OSError` occurs, the function re-raises the exception.

    Parameters:
    - path (str): The path where the directory will be created.

    Raises:
    - OSError: Any exception raised by `os.makedirs` other than "directory already exists".
    """
    if len(path) > 0:
        try:
            os.makedirs(path)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise exception


def print_error(error, context="Line", context_str=""):
    """
    Print an error message and terminate the program.

    This function formats an error message and prints it, then exits the program with status 1.

    Parameters:
    - error (str): The error message to be printed.
    - context (str): The context where the error occurred. Defaults to "Line".
    - context_str (str): A string providing additional context. Defaults to an empty string.

    The function will exit the program after printing the error message.
    """
    error_str = f"ERROR: Please check samplesheet -> {error}"
    if context != "" and context_str != "":
        error_str = f"ERROR: Please check samplesheet -> {error}\n{context.strip()}: '{context_str.strip()}'"
    print(error_str)
    sys.exit(1)


def dump_versions(process_name):
    """
    Dump the current Python version into a 'versions.yml' file.

    This function writes the process name and the current Python version into a YAML file named 'versions.yml'.

    Parameters:
    - process_name (str): A string representing the name of the process to be included in the file.
    """
    with open("versions.yml", "w", encoding="UTF-8") as out_f:
        out_f.write(process_name + ":\n")
        out_f.write("    python: " + platform.python_version() + "\n")


def check_samplesheet(file_in, file_out):
    """
    Should have structure
    sample_name,length_perc,determinant_1,determinant_2,determinant_3,roi_ref,reads
    """
    # Init
    seen_samples = set()

    # Init input file
    with open(file_in, "r", encoding="UTF-8") as fin:
        # Init header
        header_template = ["sample_name", "length_perc", "determinant_1", "determinant_2", "determinant_3", "roi_ref", "reads", "barcode"]
        output_template = ["id", "sample_name", "length_perc", "determinant_1", "determinant_2", "determinant_3", "roi_ref", "reads", "barcode", "bc_name"]
        template_len = len(header_template)
        header = [x.strip('"') for x in fin.readline().strip().split(",")]

        # Init output file
        out_dir = os.path.dirname(file_out)
        make_dir(out_dir)
        with open(file_out, "w", encoding="UTF-8") as fout:
            # Init header
            fout.write(",".join(output_template) + "\n")

            # Check sample lines
            line_no = 2
            for line in fin:
                line_proc = [x.strip().strip('"') for x in line.strip().split(",")]

                print(line_proc)

                # Check if its just a blank line so we dont error
                if line.strip() == "":
                    continue

                # Check header size
                if len(line_proc) != template_len:
                    print_error(f"Line: {line_no} - Invalid number of columns (found {len(line_proc)} should be {template_len})!", line)

                # Clean sample names
                line_proc[0] = line_proc[0].replace(".", "_")
                line_proc[0] = line_proc[0].replace("/", "_")

                # Check we either have reads or barcode but not both
                if (line_proc[6] == "" and line_proc[7] == "") or (line_proc[6] != "" and line_proc[7] != ""):
                    print_error(f"Line: {line_no} - must have reads OR barcode in line", line)

                # Extract bc name
                bc = line_proc[7]
                if line_proc[6] != "":
                    bc = os.path.splitext(os.path.basename(line_proc[6]))[0].lower()

                # Extract roi name
                roi_name = os.path.splitext(os.path.basename(line_proc[5]))[0].lower()

                # Construct the sample id
                sample_id = line_proc[0] + "_" + bc + "_" + roi_name

                # Check for duplicate sample_name/bc/roi_ref pairs
                if sample_id in seen_samples:
                    print_error(f"Line: {line_no} - Duplicate sample_id/roi/bc pairs detected!", line)
                seen_samples.add(sample_id)

                # Check we have a determinant
                if line_proc[2] == "":
                    print_error(f"Line: {line_no} - Must have at least one determinant!", line)

                # Create output line
                line_proc.insert(0, sample_id)
                line_proc.append(bc)

                # Write sample line
                fout.write(",".join(line_proc) + "\n")

                # Bump line no
                line_no = line_no + 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--process_name", default="!{process_name}")
    parser.add_argument("--samplesheet", default="!{samplesheet}")
    parser.add_argument("--output", default="!{output}")
    args = parser.parse_args()

    check_samplesheet(args.samplesheet, args.output)
    dump_versions(args.process_name)
