#!/usr/bin/env nextflow
/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    CAS9POINT4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

nextflow.enable.dsl = 2

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT FUNCTIONS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

include { summary_log     } from './modules/local/logging.nf'
include { multiqc_summary } from './modules/local/logging.nf'

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    INIT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

log.info summary_log(workflow, params, params.debug, params.monochrome_logs)

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    VALIDATE INPUTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

// Check manditory input parameters to see if the files exist if they have been specified
check_param_list = [
    samplesheet: params.samplesheet
]
for (param in check_param_list) {
    if (!param.value) {
        exit 1, "Required parameter not specified: ${param.key}"
    }
    else {
        file(param.value, checkIfExists: true)
    }
}

// Check non-manditory input parameters to see if the files exist if they have been specified
check_param_list = [
    params.fast5
]
for (param in check_param_list) { if (param) { file(param, checkIfExists: true) } }

// Select dorado model
// Dorado models can be selected by auto selection (e.g. hac selects the latest compatible hac model) or by direct model selection.
// In the case of direct selection, we need to add the container path to the model.
dorado_model = params.dorado_auto_model
if(params.dorado_model) {
    dorado_model = "/home/" + params.dorado_model
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    CONFIG FILES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

ch_multiqc_config = file("$projectDir/assets/multiqc_config.yml", checkIfExists: true)

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT LOCAL MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

//
// MODULEs
//

include { LINUX_COMMAND as MERGE_DETERMINANTS } from './modules/local/linux/command/main'
include { SEQTK_HRUN                          } from './modules/local/seqtk/hrun/main'
include { LINUX_COMMAND as FORMAT_BED         } from './modules/local/linux/command/main'
include { FILTER_BAM_RATIO                    } from './modules/local/python/filter_bam_ratio/main'
include { FILTER_BAM_BLAST                    } from './modules/local/python/filter_bam_blast/main'
include { SUBSAMPLE_BAM_COV                   } from './modules/local/python/subsample_bam_cov/main'
include { MEDAKA_VARIANT                      } from './modules/local/medaka/medaka_variant/main'
include { CLAIR3_CALL                         } from './modules/local/clair3/call/main'
include { SEQTK_SEQ                           } from './modules/local/seqtk/seq/main'
include { PYSAMSTATS                          } from './modules/local/pysamstats/main'
include { GEN_REPORTS                         } from './modules/local/python/gen_reports/main'
include { DUMP_SOFTWARE_VERSIONS              } from './modules/local/dump_software_versions.nf'

//
// SUBWORKFLOWS
//

include { PARSE_SAMPLESHEET } from './subworkflows/local/parse_samplesheet.nf'
include { DEMULTIPLEX_FAST5 } from './subworkflows/local/demultiplex_fast5.nf'

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    IMPORT NF-CORE MODULES/SUBWORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

//
// MODULEs
//

include { SAMTOOLS_FAIDX                           } from './modules/nf-core/samtools/faidx/main'
include { CUSTOM_GETCHROMSIZES as DET_SIZES        } from './modules/nf-core/custom/getchromsizes/main'
include { CUSTOM_GETCHROMSIZES as ROI_SIZES        } from './modules/nf-core/custom/getchromsizes/main'
include { BEDTOOLS_SLOP                            } from './modules/nf-core/bedtools/slop/main'
include { FASTQC                                   } from './modules/nf-core/fastqc/main'
include { NANOPLOT as NANOPLOT_FASTQ               } from './modules/nf-core/nanoplot/main'
include { FILTLONG                                 } from './modules/nf-core/filtlong/main'
include { BLAST_MAKEBLASTDB                        } from './modules/nf-core/blast/makeblastdb/main'
include { MINIMAP2_ALIGN                           } from './modules/nf-core/minimap2/align/main'
include { SAMTOOLS_INDEX as SAMTOOLS_INDEX_ALIGN   } from './modules/nf-core/samtools/index/main'
include { SAMTOOLS_VIEW                            } from './modules/nf-core/samtools/view/main'
include { SAMTOOLS_INDEX as SAMTOOLS_INDEX_FILTSMP } from './modules/nf-core/samtools/index/main'
include { BLAST_BLASTN as BLAST_ROI_REF            } from './modules/nf-core/blast/blastn/main'
include { BLAST_BLASTN as BLAST_READS              } from './modules/nf-core/blast/blastn/main'
include { SAMTOOLS_DEPTH as SAMTOOLS_DEPTH_PASSED  } from './modules/nf-core/samtools/depth/main'
include { BEDTOOLS_INTERSECT                       } from './modules/nf-core/bedtools/intersect/main'
include { SNIFFLES                                 } from './modules/nf-core/sniffles/main'
include { SAMTOOLS_DEPTH as SAMTOOLS_DEPTH_ALIGNED } from './modules/nf-core/samtools/depth/main'
include { UNTAR as UNTAR_MODEL                     } from './modules/nf-core/untar/main'
include { MULTIQC                                  } from './modules/nf-core/multiqc/main'

//
// SUBWORKFLOWS
//

include { BAM_SORT_STATS_SAMTOOLS                          } from './subworkflows/nf-core/bam_sort_stats_samtools/main'
include { BAM_STATS_SAMTOOLS as BAM_STATS_ALIGN            } from './subworkflows/nf-core/bam_sort_stats_samtools/main'
include { BAM_SORT_STATS_SAMTOOLS as BAM_SORT_STATS_PASSED } from './subworkflows/nf-core/bam_sort_stats_samtools/main'


/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN MAIN WORKFLOW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

workflow CAS9POINT4 {
    // Init
    ch_versions     = Channel.empty()
    ch_samplesheet  = file(params.samplesheet, checkIfExists: true)
    ch_clair3_model = []
    if(params.clair3_model_path) {
        ch_clair3_model = Channel.from(file(params.clair3_model_path, checkIfExists: true))
    }

    //
    // ****************************
    //
    // SECTION: Input Checking
    //
    // ****************************
    //
    if(params.run_input_check) {
        //
        // SUBWORKFLOW: Parse the samplesheet file into metadata and check for errors
        //
        PARSE_SAMPLESHEET (
            ch_samplesheet
        )
        ch_versions = ch_versions.mix(PARSE_SAMPLESHEET.out.versions)
        ch_data     = PARSE_SAMPLESHEET.out.data
        ch_reads    = PARSE_SAMPLESHEET.out.reads
        ch_rois     = PARSE_SAMPLESHEET.out.roi
        ch_dets     = PARSE_SAMPLESHEET.out.dets
    }

    //
    // ****************************
    //
    // SECTION: Convert fast5 to fastq if required
    //
    // ****************************
    //
    if(params.run_conversion && params.fast5) {
        //
        // SUBWORKFLOW: Decompress, call bases and sort meta data
        //
        DEMULTIPLEX_FAST5 (
            ch_data,
            params.fast5,
            dorado_model,
            params.dorado_bc_kit,
            params.pod5_mode
        )
        ch_versions = ch_versions.mix(DEMULTIPLEX_FAST5.out.versions)
        ch_data     = DEMULTIPLEX_FAST5.out.data
        ch_reads    = DEMULTIPLEX_FAST5.out.reads
    }

    //
    // ****************************
    //
    // SECTION: Reference processing
    //
    // ****************************
    //
    if(params.run_ref_prep) {
        //
        // CHANNEL: Get unique sorted list of determinants
        //
        ch_unique_dets_list = ch_dets
            .map{it[1]}
            .flatten()
            .unique()
            .toSortedList()
            .map{[[id:"determinants"], it]}

        //
        // CHANNEL: Get unique determinants as a channel list
        //
        ch_unique_dets = ch_dets
            .map{it[1]}
            .flatten()
            .unique()
            .map{[[id:"determinants"], it]}

        //
        // MODULE: Concat the determinant files into one file
        //
        MERGE_DETERMINANTS (
            ch_unique_dets_list,
            [],
            false
        )
        // ch_versions    = ch_versions.mix(CAT_CAT.out.versions)
        ch_merged_dets = MERGE_DETERMINANTS.out.file

        //
        // MODULE: Build blastdb of merged determinant sequences
        //
        BLAST_MAKEBLASTDB (
            ch_merged_dets.map{it[1]}
        )
        ch_versions    = ch_versions.mix(BLAST_MAKEBLASTDB.out.versions)
        ch_det_blastdb = BLAST_MAKEBLASTDB.out.db

        //
        // MODULE: Calculate sizes of the determinants
        //
        DET_SIZES (
            ch_unique_dets
        )
        ch_versions  = ch_versions.mix(DET_SIZES.out.versions)
        ch_det_sizes = DET_SIZES.out.sizes

        //
        // CHANNEL: Add det sizes to metadata
        //
        ch_det_meta = ch_det_sizes
            .splitCsv(header: ['id', 'det_size'], sep: "\t" )
            .map{ [it[1].id.toLowerCase(), [id:it[1].id.toLowerCase(), det_size:it[1].det_size]] }
        ch_unique_dets = ch_unique_dets
            .map{ [it[1].simpleName.toLowerCase(), [id:it[1].simpleName.toLowerCase()], it[1]] }
            .join(ch_det_meta, remainder:true)
            .map{ it ->
                if(it[3] == null) {
                     exit 1, "Determinant filename/fasta name mismatch: " + it[2]
                }
                [ it[3], it[2] ] 
            }

        //
        // CHANNEL: Get unique roi refs as a channel list
        //
        ch_unique_rois = ch_rois
            .map{ [[id:it[0].roi_name], it[1]]}
            .unique()

        //
        // MODULE: Index the ROI refs
        //
        SAMTOOLS_FAIDX (
            ch_unique_rois,
            [[], []]
        )
        ch_versions = ch_versions.mix(SAMTOOLS_FAIDX.out.versions)
        ch_roi_fai  = SAMTOOLS_FAIDX.out.fai

        //
        // MODULE: Calculate sizes of the determinants
        //
        ROI_SIZES (
            ch_unique_rois
        )
        ch_versions  = ch_versions.mix(ROI_SIZES.out.versions)
        ch_roi_sizes = ROI_SIZES.out.sizes

        //
        // CHANNEL: Add roi sizes to metadata
        //
        ch_roi_meta = ch_roi_sizes
            .splitCsv(header: ['id', 'roi_size'], sep: "\t" )
            .map{ [it[1].id.toLowerCase(), [id:it[1].id.toLowerCase(), roi_size:it[1].roi_size]] }
        ch_unique_rois = ch_unique_rois
            .map{ [it[0].id, it[0], it[1]] }
            .join(ch_roi_meta, remainder: true)
            .map{ it ->
                if(it[3] == null) {
                     exit 1, "ROI Ref filename/fasta name mismatch: " + it[2]
                }
                [ it[3], it[2] ] 
            }

        //
        // MODULE: Scan roi ref files for homopolymers
        //
        SEQTK_HRUN (
            ch_unique_rois
        )
        ch_versions = ch_versions.mix(SEQTK_HRUN.out.versions)
        ch_roi_hrun_bed = SEQTK_HRUN.out.bed

        //
        // MODULE: Sort and format the bed file to just 3 columns
        //
        FORMAT_BED (
            ch_roi_hrun_bed,
            [],
            false
        )
        ch_roi_hrun_bed = FORMAT_BED.out.file

        //
        // CHANNEL: Combine roi fasta and size files
        //
        ch_roi_bed_sizes = ch_roi_hrun_bed
            .map{ [ it[0].id, it[0], it[1] ] }
            .join( ch_roi_sizes.map{ [it[0].id, it[1]] } )
            .map { [ it[1], it[2], it[3] ] }

        //
        // MODULE: Add 1 to each end of the bed ranges
        //
        BEDTOOLS_SLOP (
            ch_roi_bed_sizes.map{[it[0], it[1]]},
            ch_roi_bed_sizes.map{it[2]},
        )
        ch_versions = ch_versions.mix(BEDTOOLS_SLOP.out.versions)
        ch_roi_hrun_bed = BEDTOOLS_SLOP.out.bed
    }

    //
    // ****************************
    //
    // SECTION: Initial QC
    //
    // ****************************
    //
    if(params.run_qc) {
        //
        // MODULE: Run Fastqc on reads
        //
        // FASTQC (
        //     ch_reads
        // )
        // ch_versions = ch_versions.mix(FASTQC.out.versions)

        //
        // MODULE: Run nanoplot on reads
        //
        NANOPLOT_FASTQ (
            ch_reads
        )
        ch_versions = ch_versions.mix(NANOPLOT_FASTQ.out.versions)
    }

    //
    // ****************************
    //
    // SECTION: Read Filtering
    //
    // ****************************
    //
    ch_filtlong_log = Channel.empty()
    if(params.run_read_filter) {
        //
        // MODULE: Filter long reads
        //
        FILTLONG (
            ch_reads.map{ [ it[0], [], it[1]] }
        )
        ch_versions     = ch_versions.mix(FILTLONG.out.versions)
        ch_reads        = FILTLONG.out.reads
        ch_filtlong_log = FILTLONG.out.log
    }

    //
    // ****************************
    //
    // SECTION: Alignment and fragment processing
    //
    // ****************************
    //
    ch_bam_stats          = Channel.empty()
    ch_bam_flagstat       = Channel.empty()
    ch_bam_idxstats       = Channel.empty()
    ch_bam_align          = Channel.empty()
    ch_bam_bai_align      = Channel.empty()
    ch_bam_stats_align    = Channel.empty()
    ch_bam_flagstat_align = Channel.empty()
    ch_bam_idxstats_align = Channel.empty()
    if(params.run_alignment) {
        //
        // Channel: Combine reads and roi references
        //
        ch_reads_roi = ch_reads
            .map{ [ it[0].id, it[0], it[1] ] }
            .join( ch_rois.map{ [it[0].id, it[1]] } )
            .map { [ it[1], it[2], it[3] ] }

        //
        // MODULE: Align to roi reference using minimap2
        //
        MINIMAP2_ALIGN (
            ch_reads_roi.map{ [it[0], it[1]] },
            ch_reads_roi.map{ it[2] },
            true,
            false,
            false
        )
        ch_versions  = ch_versions.mix(MINIMAP2_ALIGN.out.versions)
        ch_bam       = MINIMAP2_ALIGN.out.bam
        ch_bam_align = MINIMAP2_ALIGN.out.bam

        //
        // MODULE: Index the bam file (already sorted by MINIMAP2)
        //
        SAMTOOLS_INDEX_ALIGN (
            ch_bam,
        )
        ch_versions = ch_versions.mix(SAMTOOLS_INDEX_ALIGN.out.versions)
        ch_bai      = SAMTOOLS_INDEX_ALIGN.out.bai

        //
        // CHANNEL: Combine BAM and BAI
        //
        ch_bam_bai = ch_bam
            .join(ch_bai, by: [0])
            .map {
                meta, bam, bai ->
                    if (bai) {
                        [ meta, bam, bai ]
                    }
            }
        ch_bam_bai_align = ch_bam_bai

        //
        // SUBWORKFLOW: Calculate stats on BAM
        //
        if(params.calc_intermed_stats) {
            BAM_STATS_ALIGN (
                ch_bam_bai,
                [[], []]
            )
            ch_bam_stats_align    = BAM_STATS_ALIGN.out.stats
            ch_bam_flagstat_align = BAM_STATS_ALIGN.out.flagstat
            ch_bam_idxstats_align = BAM_STATS_ALIGN.out.idxstats
        }

        //
        // MODULE: filter reads for mapping quality, secondary alignments and supplimentary alignments
        //
        SAMTOOLS_VIEW (
            ch_bam_bai,
            [[],[]],
            []
        )
        ch_versions = ch_versions.mix(SAMTOOLS_VIEW.out.versions)
        ch_bam      = SAMTOOLS_VIEW.out.bam

        //
        // CHANNEL: Filter empty bams
        //
        ch_bam = ch_bam.filter { row ->
            file(row[1]).size() >= params.min_bam_size
        }

        //
        // MODULE: Index the bam file after filtering
        //
        SAMTOOLS_INDEX_FILTSMP (
            ch_bam,
        )
        ch_versions = ch_versions.mix(SAMTOOLS_INDEX_FILTSMP.out.versions)
        ch_bai      = SAMTOOLS_INDEX_FILTSMP.out.bai

        //
        // CHANNEL: Combine BAM and BAI
        //
        ch_bam_bai = ch_bam
            .join(ch_bai, by: [0])
            .map {
                meta, bam, bai ->
                    if (bai) {
                        [ meta, bam, bai ]
                    }
            }

        //
        // MODULE: futher filter reads using python
        // For each read:
        //   1. Compute the length of the read
        //   2. Extract the alignment length from the CIGAR string
        //   3. Extract the 'NM' tag value (number of mismatches) from the read
        //   4. Calculate the alignment length ratio (alignment length / read length)
        //   5. Calculate the NM ratio (number of mismatches / alignment length)
        //   6. Write the read if it meets the specified alignment length ratio and NM ratio criteria
        FILTER_BAM_RATIO (
            ch_bam_bai,
            params.align_ratio_thresh,
            params.mismatch_ratio_thresh
        )
        ch_versions = ch_versions.mix(FILTER_BAM_RATIO.out.versions)
        ch_bam      = FILTER_BAM_RATIO.out.bam

        //
        // SUBWORKFLOW: Calculate stats for aligned reads
        //
        BAM_SORT_STATS_SAMTOOLS (
            ch_bam,
            [[], []]
        )
        ch_versions     = ch_versions.mix(BAM_SORT_STATS_SAMTOOLS.out.versions)
        ch_bam          = BAM_SORT_STATS_SAMTOOLS.out.bam
        ch_bai          = BAM_SORT_STATS_SAMTOOLS.out.bai
        ch_bam_stats    = BAM_SORT_STATS_SAMTOOLS.out.stats
        ch_bam_flagstat = BAM_SORT_STATS_SAMTOOLS.out.flagstat
        ch_bam_idxstats = BAM_SORT_STATS_SAMTOOLS.out.idxstats

        //
        // CHANNEL: Filter empty bams
        //
        ch_bam = ch_bam.filter { row ->
            file(row[1]).size() >= params.min_bam_size
        }

        //
        // CHANNEL: Combine BAM and BAI
        //
        ch_bam_bai = ch_bam
            .join(ch_bai, by: [0])
            .map {
                meta, bam, bai ->
                    if (bai) {
                        [ meta, bam, bai ]
                    }
            }
    }

    //
    // ****************************
    //
    // SECTION: Blast and blast-dependant processing
    //
    // ****************************
    //
    if(params.run_blast) {
        //
        // MODULE: Search roi references against determinants database
        //
        BLAST_ROI_REF (
            ch_unique_rois,
            ch_det_blastdb
        )
        ch_versions  = ch_versions.mix(BLAST_ROI_REF.out.versions)
        ch_roi_blast = BLAST_ROI_REF.out.txt

        //
        // MODULE: Convert fastq reads to fasta
        //
        SEQTK_SEQ (
            ch_reads
        )
        ch_versions   = ch_versions.mix(SEQTK_SEQ.out.versions)
        ch_read_fasta = SEQTK_SEQ.out.fastx

        //
        // MODULE: Search reads against determinants database
        //
        BLAST_READS (
            ch_read_fasta,
            ch_det_blastdb
        )
        ch_versions   = ch_versions.mix(BLAST_READS.out.versions)
        ch_read_blast = BLAST_READS.out.txt

        //
        // CHANNEL: Prepare channel for filtering the bam files based on blast results
        //
        ch_bam_bai_blast = ch_bam_bai
            .map{ [ it[0].id, it[0], it[1], it[2] ] }
            .join( ch_read_blast.map{ [it[0].id, it[1]] } )
            .join( ch_dets.map{ [it[0].id, it[1]] } )
            .combine( ch_unique_dets.collect{it[0]}.map{[it]} )
            .map { row ->
                def det_names = row[5].collect{ it.simpleName.toLowerCase() }
                def det_sizes = det_names.collect { name ->
                    row[6].find { it.id == name }?.det_size
                }
                [ row[1], row[2], row[3], row[4], det_names.join(','), det_sizes.join(',') ]
            }

        //
        // MODULE: Futher filter reads based on the blast results
        //
        FILTER_BAM_BLAST (
            ch_bam_bai_blast.map{ [it[0], it[1], it[2], it[3]] },
            ch_bam_bai_blast.map{ it[4] },
            ch_bam_bai_blast.map{ it[5] }
        )
        ch_versions   = ch_versions.mix(FILTER_BAM_BLAST.out.versions)
        ch_passed_bam = FILTER_BAM_BLAST.out.passed_bam
        ch_failed_bam = FILTER_BAM_BLAST.out.failed_bam

        //
        // SUBWORKFLOW: Calculate stats for passed reads
        //
        BAM_SORT_STATS_PASSED (
            ch_passed_bam,
            [[], []]
        )
        ch_versions     = ch_versions.mix(BAM_SORT_STATS_PASSED.out.versions)
        ch_bam          = BAM_SORT_STATS_PASSED.out.bam
        ch_bai          = BAM_SORT_STATS_PASSED.out.bai
        ch_bam_stats    = BAM_SORT_STATS_PASSED.out.stats
        ch_bam_flagstat = BAM_SORT_STATS_PASSED.out.flagstat
        ch_bam_idxstats = BAM_SORT_STATS_PASSED.out.idxstats

        //
        // CHANNEL: Filter empty bams
        //
        ch_bam = ch_bam.filter { row ->
            file(row[1]).size() >= params.min_bam_size
        }
    }

    //
    // ****************************
    //
    // SECTION: Variant Calculation
    //
    // ****************************
    //
    if(params.run_variant_calc) {
        //
        // MODULE: Calculate depth of passed reads
        // 
        SAMTOOLS_DEPTH_PASSED  (
            ch_bam,
            [[], []]
        )
        ch_versions         = ch_versions.mix(SAMTOOLS_DEPTH_PASSED.out.versions)
        ch_passed_bam_depth = SAMTOOLS_DEPTH_PASSED.out.tsv

        //
        // CHANNEL: Combine bam with depth
        //
        ch_bam_depth = ch_bam
            .join(ch_passed_bam_depth, by: [0])
            .map {
                meta, bam, depth ->
                [ meta, bam, depth ]
            }

        if(params.var_enable_subsampling) {
            //
            // MODULE: Subsample reads to standardise coverage across samples
            // 
            SUBSAMPLE_BAM_COV (
                ch_bam_depth,
                params.var_coverage_thresh
            )
            ch_versions = ch_versions.mix(SUBSAMPLE_BAM_COV.out.versions)
            ch_bam      = SUBSAMPLE_BAM_COV.out.bam
            ch_bai      = SUBSAMPLE_BAM_COV.out.bai
        }

        //
        // CHANNEL: Combine bam and bai
        //
        ch_bam_bai = ch_bam
            .join(ch_bai, by: [0])
            .map {
                meta, bam, bai ->
                    if (bai) {
                        [ meta, bam, bai ]
                    }
            }

        //
        // CHANNEL: Combine bam and bai with roi
        //
        ch_bam_bai_roi = ch_bam_bai
            .map{ [ it[0].id, it[0], it[1], it[2] ] }
            .join( ch_rois.map{ [it[0].id, it[1]] } )
            .map { [ it[1], it[2], it[3], it[4] ] }

        //
        // MODULE: Call variants using medaka
        //
        MEDAKA_VARIANT (
            ch_bam_bai_roi
        )
        ch_versions = ch_versions.mix(MEDAKA_VARIANT.out.versions)

        //
        // CHANNEL: Combine vcf with homopolymer calc
        //
        ch_vcf_hp_bed = MEDAKA_VARIANT.out.vcf
            .flatMap{ item ->
                item[1].collect { file -> [item[0].roi_name, item[0], file] }
            }
            .combine(ch_roi_hrun_bed.map { [it[0].id, it[1]] }, by: 0)
            .map { [ it[1], it[2], it[3] ] }

        //
        // MODULE: Filter the VCF against homopolymer regions
        // 
        BEDTOOLS_INTERSECT (
            ch_vcf_hp_bed,
            [[], []]
        )
        ch_versions = ch_versions.mix(BEDTOOLS_INTERSECT.out.versions)

        //
        // MODULE: Run sniffles 
        //
        SNIFFLES (
            ch_bam_bai_roi.map{[it[0], it[1], it[2]]},
            ch_bam_bai_roi.map{[it[0], it[3]]}
        )
        ch_versions = ch_versions.mix(SNIFFLES.out.versions)

        //
        // CHANNEL: Combine bam, bai, ref, fai
        //
        ch_bam_bai_roi_fai = ch_bam_bai_roi
            .map{ [ it[0].roi_name, it[0], it[1], it[2], it[3] ] }
            .combine(ch_roi_fai.map { [it[0].id, it[1]] }, by: 0)
            .map { [ it[1], it[2], it[3], it[4], it[5] ] }

        //
        // MODULE: Untar clair3 model if required
        //
        if(params.clair3_model_path) {
            UNTAR_MODEL (
                ch_clair3_model.map{[[:], it]}
            )
            ch_clair3_model = UNTAR_MODEL.out.untar.map{it[1]}.collect()
        }

        //
        // MODULE: Run Clair3 
        //
        CLAIR3_CALL (
            ch_bam_bai_roi_fai,
            params.clair3_model,
            ch_clair3_model
        )
    }

    //
    // ****************************
    //
    // SECTION: Report Generation
    //
    // ****************************
    //
    if(params.run_reporting) {
        //
        // CHANNEL: Combine bam, bai and roi from initial align
        //
        ch_bam_bai_roi_align = ch_bam_bai_align
            .map{ [ it[0].id, it[0], it[1], it[2] ] }
            .join( ch_rois.map{ [it[0].id, it[1]] } )
            .map { [ it[1], it[2], it[3], it[4] ] }

        //
        // MODULE: Calculate coverage depth from raw aligned reads
        //
        SAMTOOLS_DEPTH_ALIGNED (
            ch_bam_align,
            [[], []]
        )

        //
        // MODULE: Calculate strand variations
        //
        PYSAMSTATS (
            ch_bam_bai_roi_align.map{[it[0], it[1], it[2]]},
            ch_bam_bai_roi_align.map{[it[3]]}
        )

        //
        // MODULE: Generate plots and multiqc inputs
        //
        GEN_REPORTS (
            SAMTOOLS_DEPTH_ALIGNED.out.tsv.collect{it[1]},
            PYSAMSTATS.out.pysam.collect{it[1]}
        )

        //
        // MODULE: Collect software versions
        //
        DUMP_SOFTWARE_VERSIONS (
            ch_versions.unique().collectFile()
        )

        //
        // MODULE: Run multiqc
        //
        workflow_summary    = multiqc_summary(workflow, params)
        ch_workflow_summary = Channel.value(workflow_summary)

        ch_multiqc_files = Channel.empty()
        ch_multiqc_files = ch_multiqc_files.mix(ch_workflow_summary.collectFile(name: 'workflow_summary_mqc.yaml'))
        ch_multiqc_files = ch_multiqc_files.mix(DUMP_SOFTWARE_VERSIONS.out.mqc_yml.collect())
        ch_multiqc_files = ch_multiqc_files.mix(DUMP_SOFTWARE_VERSIONS.out.mqc_unique_yml.collect())

        // ch_multiqc_files = ch_multiqc_files.mix(FASTQC.out.zip.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(NANOPLOT_FASTQ.out.txt.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_filtlong_log.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_stats_align.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_flagstat_align.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_idxstats_align.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_stats.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_flagstat.collect{it[1]}.ifEmpty([]))
        ch_multiqc_files = ch_multiqc_files.mix(ch_bam_idxstats.collect{it[1]}.ifEmpty([]))

        MULTIQC (
            ch_multiqc_files.collect(),
            ch_multiqc_config,
            [],
            []
        )
        multiqc_report = MULTIQC.out.report.toList()
    }

}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN MAIN WORKFLOW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

workflow {
    CAS9POINT4()
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    COMPLETION EVENTS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

// workflow.onComplete {
// }

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
