params.base="$baseDir"
inExp = params.inExp
fast=Channel.fromPath(params.inFiles)
params.refDir="${params}.baseDir" + 'refs/'
params.determinantsCsv='determinants_template.csv'
determinants=file(params.determinantsCsv)
dFasta=file(params.determinantsFasta)
determinantsFasta=Channel.from( dFasta )

params.basecall=false
params.gpu=false
if (params.gpu == true) { 
	gpu_device = "--device cuda:0"
} else {
	gpu_device = ""
}

params.basecaller_model="dna_r9.4.1_450bps_hac.cfg"
bc_model=params.basecaller_model

params.demultiplex=false
params.require_barcodes_both_ends=true
if (params.require_barcodes_both_ends == true) {
	both_ends = "--require_barcodes_both_ends"
} else {
	both_ends = ""
}
params.barcode_kit='"EXP-PBC001"'
barcode_kit=params.barcode_kit
params.qual=90
params.min_seq_length=0
msl=params.min_seq_length
quals= Channel.from( params.qual )

params.align_fraction = 0.725

params.vc_covs=2000
vc_covs=params.vc_covs
params.medaka_model="r941_min_high_g360"
med_model=params.medaka_model

params.homopol_length=5
hpl=params.homopol_length


Channel
        .from( determinants )
        .splitCsv()
        .map { row -> tuple(row[1],row[10]) }
        .view()
        .set{ metaData }


if (params.basecall == true) {
process basecall {
	tag { run }

	publishDir 'basecalled', mode: 'link', pattern: '*fastq*'
	cpus 4	

	input:
	//set val(run), val(fast5s) from ginputs1
        val(run) from inExp
	file fast5s from fast
	

	output:
	set val(run), file(fast5s), file(fastqs) into basecalled,basecalled2

	script:
	"""
	guppy_basecaller -i $fast5s -c /opt/ont/ont-guppy/data/${bc_model} -s fastqs -r --num_callers 4 ${gpu_device}
	"""

					
}}




if (params.demultiplex == true) {
if (params.basecall == true) {

process gup_bar {
	tag { run }

	publishDir 'Demultiplexed_fastqs'

	cpus 4

	input:
	set val(run), val(fast5s), file(fastqs) from basecalled

	output:
	file("guppy_barcoder_fastqs/barcode*") into demulti_adaptorchop

	
	script:
        """
        guppy_barcoder -i $fastqs -s guppy_barcoder_fastqs -t ${task.cpus} -r ${both_ends} --trim_barcodes --detect_mid_strand_barcodes --barcode_kit ${barcode_kit} ${gpu_device}
        """
}}

else if(params.basecall == false) {
process guppy_demultiplexnb {
	tag { run }

	publishDir 'Demultiplexed_fastqs'

	cpus 4

	input:
	val(run) from inExp
	val(fastqs) from fast
	val(be) from both_ends

	output:
	file("guppy_barcoder_fastqs/barcode*") into demulti_adaptorchop

	
	script:
	"""
	guppy_barcoder -i $fastqs -s guppy_barcoder_fastqs -t ${task.cpus} -r ${both_ends} --trim_barcodes --detect_mid_strand_barcodes --detect_mid_strand_barcodes --barcode_kit ${barcode_kit} ${gpu_device}
	"""

}
}}

if (params.demultiplex == true) {
	
	demultiplexed= demulti_adaptorchop.flatten()
} else {
	demultiplexed = Channel
			.fromPath( params.inFiles + '/*.fastq' )
			.flatten()
}


process filtLong {
        tag { bc + ' ' + q }
	//conda 'bioconda::filtlong'
	publishDir 'filtLong'
	cpus 1

	input:
	tuple file(fastq),val(q) from demultiplexed.combine(quals)

	output:	
	tuple val(bc), file("${bc}_q${q}.fastq.gz"),val(q) into filtLonged
	

	script:
	if (params.demultiplex == false)
		bc = fastq.baseName.replace(".fastq","")
	else if (params.demultiplex == true)
		bc=fastq.baseName.replace("barcode", "BC")
	

	if (params.demultiplex == false)
		"""		
		filtlong --min_mean_q $q  ${bc}.fastq | gzip > ${bc}_q${q}.fastq.gz
		"""	
	else if (params.demultiplex == true)
		"""		
		cat ${fastq}/*.fastq > ${bc}.fastq
		filtlong --min_mean_q $q  ${bc}.fastq | gzip > ${bc}_q${q}.fastq.gz
		"""

	
}



process makeBlastDB {
	input:
	file('determinants.fa') from determinantsFasta
	
	output:
	tuple file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') into blastdb,blastdb2

	script:
	"""
	makeblastdb -in determinants.fa -dbtype nucl
	"""

}

process preRef {
	tag { bc + ' ' + d + ' ' + ref}
        //conda 'bioconda::blast'

        input:
        tuple val(bc), file("${bc}_${q}.fastq.gz"),val(q), val(ref), file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from filtLonged.combine(metaData, by:0).combine(blastdb)

        output:
        tuple val(bc), file("${bc}_${q}.fastq.gz"),val(q),val(ref),file("*.fas"),file("${ref}.blast") into refs

        script:
        d=params.refDir
        """
        echo
        cp $d/${ref} ./
        blastn -query ${ref} -db determinants.fa -outfmt 6 -out ${ref}.blast 
        """
}




process map {
        tag { bc + '_' + r}
        //conda 'bioconda::minimap2 bioconda::samtools bioconda::pysam'
	cpus 4

	publishDir 'all', mode: 'copy', overwrite: true, pattern: '*.bam*'

	input:
        tuple val(bc), file('fastq'),val(q),val(r),file('ref'),file("${r}.blast") from refs


	output:
        tuple val(bc), val(r), val(exp), file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"),file('fastq'),file('ref'),file("${r}.blast"),val(q) into assembled1,assembled2
	
	script:
        exp=params.inExp
	
	"""
	minimap2 -ax map-ont $ref fastq  |\
	samtools view -bS -q 50 - | samtools sort -o out.bam 

        samtools index out.bam
        filterBam.py out.bam ${exp}_${bc}_${r}_q${q}.sorted.bam 0.50 0.3
	samtools index ${exp}_${bc}_${r}_q${q}.sorted.bam
	"""

}



process bamStats {
	tag { bc + '_' + r}
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats'

	publishDir 'stats', mode: 'copy', overwrite: true, pattern: '*.tsv'
        publishDir 'depthCharts', mode: 'copy', overwrite: true, pattern: '*depth.pdf'
        publishDir 'covBases', mode: 'copy', overwrite: true, pattern: '*covbases.pdf'
        publishDir 'psyamStats', mode: 'copy', overwrite: true, pattern: '*.pysam'
        

	input:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q) from assembled1

	output:
        file("*idxstats.tsv") into bstats
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv") into depths
        file("*.pdf") into depthCharts
        file("*.pysam") into pysamStats

	script:
	"""
	samtools idxstats ${exp}_${bc}_${r}_q${q}.sorted.bam > ${exp}_${bc}_${r}_${q}.idxstats.tsv
        samtools depth -m 0 -aa ${exp}_${bc}_${r}_q${q}.sorted.bam > ${exp}_${bc}_${r}_${q}.depth.tsv
        plotDepth.py ${exp}_${bc}_${r}_${q}.depth.tsv
	pysamstats -t variation_strand -f ref -d ${exp}_${bc}_${r}_q${q}.sorted.bam -D 300000 > ${exp}_${bc}_${r}_${q}.pysam
        covBases.py ${exp}_${bc}_${r}_${q}.pysam ${exp}_${bc}_${r}_${q}

	"""
}





process vcf_filt {
	errorStrategy 'ignore'
	tag { bc + '_' + r }
	//conda 'bioconda:: seqtk bedops dustmasker bedtools'
	publishDir 'Bed_files' , mode: 'copy', overwrite: true, pattern: '*filt.bed'
	
	input: 
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv") from depths
	
	output:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed") into bed_varcall

	
	script:
	hpl=params.homopol_length

	"""
	seqtk hrun ref ${hpl} > ${r}_seqtk.bed
	sort -k1,1 -k2,2n ${r}_seqtk.bed | cut -f1,2,3 > cat.sorted.bed
	bedops --range -1:1 --everything cat.sorted.bed > ${r}_${bc}_filt.bed
	"""

}




process blastRawReads {
	tag { bc + '_' + r}
        //conda 'bioconda::blast'

	input:
	
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file('determinants.fa'), file('determinants.fa.nhr'), file('determinants.fa.nin'), file('determinants.fa.nsq') from bed_varcall.combine(blastdb2)

	output:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("${bc}.blast") into rblast

        script:
        """
	zcat <  fastq | sed '/^@/!d;s//>/;N' > fasta
        blastn -query fasta -db determinants.fa -outfmt 6 -out ${bc}.blast 
        """

}


process filterByDeterminants {
        errorStrategy 'ignore'
	tag { bc + '_' + r }
        //conda 'bioconda::samtools pandas seaborn matplotlib pysamstats pysam'

        publishDir 'passFailPlots', mode: 'copy', overwrite: true, pattern: '*.pdf'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam'
        publishDir 'passBams', mode: 'copy', overwrite: true, pattern: '*passed.bam.bai'

	input:
	tuple val(bc), val(r), val(exp),file("${exp}_${bc}_${r}_q${q}.sorted.bam"), file("${exp}_${bc}_${r}_q${q}.sorted.bam.bai"), file('fastq'), file('ref'),file("${r}.blast"),val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("${bc}.blast") from rblast

	output:
        file("*.pdf") optional true into loxPmixs
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") optional true into loxPmixBams1
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") optional true into loxPmixBams2

	script:
	d=file(params.determinantsCsv)
	"""
	crisont.py -b "${bc}.blast" -s \
			"${exp}_${bc}_${r}_q${q}.sorted.bam" \
			-r ref -rb ${r}.blast \
			-d $d \
			--barcode ${bc}

	"""

}




process passed_medaka {
	errorStrategy 'ignore'
	tag { bc + '_' + r  + '_' + rd}
	//conda 'bioconda::medaka samtools', pattern: 'medaka*'

	publishDir 'passBam_medaka' , mode: 'copy', overwrite: true, pattern: 'medaka*'
	

	input: 
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") from loxPmixBams1
	
	output:
	file("*medaka*") optional true into passed_med

	script:
	rd=params.vc_covs
	"""
	if [ \$(samtools view -c passed.bam) -gt 0 ]
	then	
		samtools depth -m 0 -aa passed.bam > depth.tsv
		cov_reduction.py passed.bam reduction.bam $rd depth.tsv
		medaka_variant -i reduction.bam -f ref -o medaka_${bc}_${r} -m ${med_model} -s ${med_model} -t 4
		bedtools intersect -a medaka_${bc}_${r}/round_1.vcf -b ${r}_${bc}_filt.bed -v -header > medaka_${r}_${bc}_filt_passBam.vcf
	fi
	"""

}

process passed_sniffles {
	errorStrategy 'ignore'
	tag { bc + '_' + r  + '_' + rd}
	//conda 'bioconda::sniffles samtools', pattern: 'sniffles*'

	publishDir 'passBam_sniffles' , mode: 'copy', overwrite: true, pattern: 'sniffles*'
	

	input: 
	tuple val(r),val(bc),val(exp), file('ref'), val(q), file("${exp}_${bc}_${r}_${q}.depth.tsv"), file("${r}_${bc}_filt.bed"), file("*passed.bam"), file("*passed.bam.bai") from loxPmixBams2
	
	output:
	file("*sniffles*") optional true into passed_sniff

	script:
	rd=params.vc_covs
	"""
        if [ \$(samtools view -c passed.bam) -gt 0 ]
        then
		samtools depth -m 0 -aa passed.bam > depth.tsv
        	cov_reduction.py passed.bam reduction.bam $rd depth.tsv
		samtools calmd -bAr reduction.bam ref > red_sniffles.bam
		samtools index red_sniffles.bam
		X=\$(samtools depth -m 0 red_sniffles.bam | awk '{sum+=\$3} END { print sum/NR/10}')
		sniffles -s \${X%.*} -m red_sniffles.bam -v sniffles_${r}_${bc}_passBam.vcf
	fi
	"""

}



