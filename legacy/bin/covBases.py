#!/usr/bin/env python3
#import matplotlib as mpl
#mpl.use('Agg')
import pandas as pd
import seaborn as sns
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys
sns.set_style('darkgrid')


def accuracy(r,base):
    if r['ref'] == r[base]:
        return 101
    else:
        return 0

def plot(f,t):
    df=pd.read_csv(f,sep='\t')
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0}_percent'.format(b)]=(df[b]/df['reads_all'])*100

    df['reads_all']=df[['A','T','C','G','deletions','insertions']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    #df['2nd_base_seq'] = df[['A','T','C','G']].apply(lambda row: row.nlargest(2)[-1],axis=1)

    df['1st'] = (df['top_base'] / df['reads_all'])*100
    #df['2nd_base'] = df[['A','T','C','G']].apply(lambda row: row.nlargest(2).values[-1],axis=1)
    #df['2nd'] = (df['2nd_base'] / df['reads_all'])*100
    #df['3rd_base'] = df[['A','T','C','G']].apply(lambda row: row.nlargest(3).values[-1],axis=1)
    #df['3rd'] = (df['3rd_base'] / df['reads_all'])*100
    #df['4th_base'] = df[['A','T','C','G']].apply(lambda row: row.nlargest(4).values[-1],axis=1)
    #df['4th'] = (df['4th_base'] / df['reads_all'])*100


    df['top_base_accuracy']=df.apply(accuracy,axis=1,args=('top_base_seq',))
    #df['2nd_base_accuracy']=df.apply(accuracy,axis=1,args=('2nd_base_seq',))
    
    #df2=df[['pos','1st','2nd','3rd','4th', 'insertions_percent','deletions_percent', 'top_base_accuracy']]
    df2=df[['pos','1st','insertions_percent','deletions_percent', 'top_base_accuracy']]
    df2=pd.melt(df2,id_vars=['pos'],value_vars=['1st','insertions_percent','deletions_percent','top_base_accuracy'])
    df2=df2.rename(columns={'variable':'base majority','value':'Percent of base in pileup'})

    g=sns.lineplot(x='pos',y='Percent of base in pileup',hue='base majority',data=df2)
    plt.title(t)
    plt.savefig('{0}_covbases.pdf'.format(t))
    #plt.show()

plot( sys.argv[1], sys.argv[2] )
