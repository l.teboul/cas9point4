#!/usr/bin/env python3
import pysam
import sys

def filterBam(inBam,outBam,t,NMR):
    samfile = pysam.AlignmentFile(inBam, "rb")
    fullreads = pysam.AlignmentFile(outBam, "wb", template=samfile)
    for read in samfile.fetch():
        rl=read.query_length
        al=read.query_alignment_length
        alrl=al/rl
        NM=read.get_tag('NM')
        NMratio = NM / float(al)
        if NMratio > float(NMR): continue
        if alrl < float(t): continue
        if read.is_secondary: continue
        if read.is_supplementary: continue
        fullreads.write(read)
    fullreads.close()
    samfile.close()

filterBam(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
