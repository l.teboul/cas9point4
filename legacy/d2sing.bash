mkdir $PWD/images
docker run \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -v $PWD/images:/output \
 --privileged -t --rm \
 singularityware/docker2singularity \
 cas9point4
