# cas9point4

Analysis workflow for ONT data from mouse CRISPR amplicon sequencing 

cas9point4 filters for potential correct amplicons from CRISPR edited animals by filtering your sample's reads for determinant sequences (e.g the insertion of a LoxP or Cre site). Any read that does not contain the determinant/s will be removed from the pileup.

The strategy aims to report only those reads that represent correct or close-to correct mutants that contain your desired modifications. Small variants are then called using medaka from the final filtered BAM file. 

## Dependencies

-nextflow

-albacore or guppy (optional)

-filtlong

-minimap2

-samtools

-nanopolish

-medaka

-bedtools

-bedops
  
-seqtk

-blast

-pysam

-pandas

-sniffles


## Installation
Assuming all the dependencies are installed, the workflow can be installed by pulling from the gitlab page. There is also the option to run with conda using the -conda flag at runtime.

```
git clone https://gitlab.com/l.teboul/cas9point4.git
```

### Docker

### Singularity

## Run 
To run, a comma seperated (.csv) determinants file is required that lists the barcodes and reference sequences for mapping. Each barcode can be assigned multiple reference sequences for which it will be aligned to.

Here is an example of the csv file:

|  sample name                    | barcode | Number of determinants | name of determinant 1 | Length of determinant 1 | name of determinant 2 | Length of determinant 2 | name of determinant 3 | Length of determinant 3 | length % | Mutant ref name  |
|---------------------------------|---------|------------------------|-----------------------|-------------------------|-----------------------|-------------------------|-----------------------|-------------------------|----------|------------------|
|  MPEG1-CRE-CAS-LINE4-B6N/1.1c   | BC01    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  MPEG1-CRE-CAS-LINE3-B6N/1.1d   | BC02    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.1h                    | BC03    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  PRO/4274.4a                    | BC04    | 1                      | Cre\_ref               | 1029                    |                       |                         |                       |                         | 75       | Mpeg1\_Cre.fas    |
|  CX3CL1-FLOX-CAS-LINE1-B6N/1.1c | BC05    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/3976.1f                    | BC06    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Cx3cl1\_Flox.fas  |
|  PRO/4264.4a                    | BC07    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PAM-FLOX-CAS-LINE1-B6N/1.1a    | BC08    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Pam\_Flox.fas     |
|  PRO/4345.5a                    | BC09    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4345.3g                    | BC10    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Prdm8\_Flox.fas   |
|  PRO/4282.4e                    | BC11    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Hnf1a\_Flox.fas   |
|  PRO/4405.4a                    | BC12    | 2                      | LoxP\_5prime           | 63                      | LoxP\_3prime           | 62                      |                       |                         | 75       | Inpp5k\_Flox.fas  |



This file can be called 'determinants_template.csv' and it will be detected automatically, or can be specified with the --determinantsFile option.

The location of the fast5 or fastq files is required with the --inFiles flag.

The location (directory of) of the reference sequence(s), correspoding to the determinants_file.csv meta data, must be specified with the --refDir option.

If choosing to filter by determinants the location (file) of the determinants sequences (multi fasta file), corresponding to the determinants_file.csv meta data, must be specified with the --determinantsFasta option.


If you have downloaded this git repo, you can run the workflow like such:


```
If you want to run 
# With basecalling (guppy (default) or albacore) and demultiplexing using the determinants based strategy
nextflow run /location/of/cas9point4/main.nf \
	--basecall true \
	--demultiplex true \
        --inFiles /location/of/fast5_files \
        --inExp exp3 \
        --refDir /location/of/soft/cas9point4/refs/ \
        --determinantsFasta /location/of/cas9point4/transgene_sequences/loxp_cre.fa \
        -profile conda 

```


## Outputs
The workflow will output a number of folders containing files generated. These include:

#### Universal outputs:

'all': Bam files before filtering by determinants, containing all the reads remaining after filtering with filtlong


'basecalled': basecalled fastq (optional true if basecall=true)


'Bed_files': folder of bed files defining homopolymeric regions for each sample defined by seqtk

'covBases': plots of coverage proportion by majority base, insertions and deletions


'depthCharts': plots of coverage depth


'filtLong': fastq file output from filtong


'Demultiplexed_fastqs': folder containing demultiplexed fastqs. If demultiplexed reads will have prefix 'barcode' (optional true if demultiplex=true)


'psyamStats': output text file from pysamstats


'stats': stats txt files from samtools depths and idxstats


'passBams': bam files only containing reads that pass the determinants file criteria


'passBam_medaka': Variants called by medaka from BAMs after being filtered for determinant sequences. Folder includes a final vcf filtered for variants in homopolymers and the complete medaka_variant output


'passBam_sniffles': Variants called by sniffles from filtered BAMs for the identification of structural changes.


'passFailPlots': plot of read depth coverage for reads that passed or failed the determinants file criteria



## Flags

```

--inExp				State experiment name or number

--inFiles			Path to fast5 or fastqs

--refDir			Path to references 

--determinantsCsv		Path to determinants meta data file	

--determinantsFasta		Path to determinants fasta sequence file

--basecall			true or false (defaut=false)

--basecaller			Decide basecaller option: guppy_cpu, guppy_gpu, albacore (default=guppy_cpu)

--basecaller_model		Model used with guppy basecaller, options can be found under /opt/ont/guppy/data/ (default=dna_r9.4.1_450bps_hac.cfg)

--demultiplex          		true or false (default=false)

--require_barcodes_both_ends	Decide when demultiplexing if only reads with forward and reverse barcodes are maintained: true or false (default=true)

--barcode_kit			Barcoding kit during library prep. Must be enclosed with "" (default="EXP-PBC001")

--qual				Quality score used by filtlong (default=90)

--medaka_model			Model used by medaka for variant calling, dependent on pore and basecaller. Full model list can be found in medaka's documentation. (default=r941_min_high_g360))

--homopol_length		Filter variant that sit in homopolymers of length N or greater (default=5)

```



## Resources 


[Medaka Documentation](https://nanoporetech.github.io/medaka/walkthrough.html)

[Sniffles Documentation](https://github.com/fritzsedlazeck/Sniffles)

[Minimap Documentation](https://github.com/lh3/minimap2)



