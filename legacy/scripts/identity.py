import sys
import pysam

def filterBam(inBam):
    samfile = pysam.AlignmentFile(inBam, "rb")
    quals=[]
    noreads=[]
    for read in samfile.fetch():
        rl=read.query_length
        al=read.query_alignment_length
        alrl=al/rl
        NM=read.get_tag('NM')
        NMratio = NM / float(al)
        quals=quals+[NMratio]
        noreads=noreads+1
    samfile.close()
    return(sum(quals)/noreads)

filterBam(sys.argv[1])
