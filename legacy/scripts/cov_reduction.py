import pysam
import sys
import pandas as pd


def sbst(inBam, outBam, rd, depth):
    out=open(outBam, "wb")
    dft = pd.read_csv(depth, sep='\t', header=None)
    av_cov = sum(dft[2]) / max(dft[1])
    red=int(rd)/av_cov
    if float(red) > 0.999999999 :
        ss=pysam.view("-s", "0.999999999", "-b", inBam)
        out.write(ss)
    else:
        ss=pysam.view("-s", str(red), "-b", inBam)
        out.write(ss)
    out.close()
    pysam.index(outBam)

sbst(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
