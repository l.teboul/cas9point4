from argparse import ArgumentParser, SUPPRESS
import seaborn as sns
import sys
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import pysam
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import pysamstats
sns.set_style('darkgrid')

# blast header names
names=['query',
    'subject',
    'id',
    'alignment length',
    'mismatches',
    'gap openings',
    'query start',
    'query end',
    'subject start',
    'subject end',
    'E value',
    'bit score']

## plotting
 
def accuracy(r,base):
    if r['ref'] == r[base]:
        return 0
    else:
        return 1

def mut_type(r,base):
    if r['ref'] == r[base]:
        return None
    else:
        return r['ref'] + r[base]

def loxPreadOr(r):
    if r['subject start'] < r['subject end']:
        return 'F'
    elif r['subject start'] > r['subject end']:
        return 'R'

def loxRefOrient(r):
    if r['flag']==0:
        if r['LoxP read orientation'] == 'F':
            return 'F'
        elif r['LoxP read orientation'] == 'R':
            return 'R'
    elif r['flag']==16:
        if r['LoxP read orientation'] == 'F':
            return 'R'
        elif r['LoxP read orientation'] == 'R':
            return 'F'

def hitStart(r):
    if r['LoxP ref orientation'] == 'F':
        return r['pos'] + r['query start']
    if r['LoxP ref orientation'] == 'R':
        return r['pos'] + r['query end']

def hitEnd(r):
    if r['LoxP ref orientation'] == 'F':
        return r['pos'] + r['query end']
    if r['LoxP ref orientation'] == 'R':
        return r['pos'] + r['query start']

def loadBlast(blast):
    df=pd.read_csv(blast,sep='\t',names=names)
    try:
        df['LoxP read orientation']=df.apply(loxPreadOr,axis=1)
    except:
        pass
    return df

def loadBam(ib):
    inBam=pysam.AlignmentFile(ib, "rb")
    for read in inBam.fetch():
        d={'query':read.query_name,
            'flag':read.flag,
            'ref':read.reference_name,
            'pos':read.reference_start,
            'mapQ':read.mapping_quality}
        yield d
    
#    df=pd.read_csv(sam,names=['query','flag','ref','pos','mapQ'])
#    return df

def mergeBamBlast(df,df2):
    df['LoxP read orientation']=df.apply(loxPreadOr,axis=1)
    df3=df2.merge(df,how='outer')
    df3['LoxP ref orientation']=df3.apply(loxRefOrient,axis=1)
    df3['Hit ref start']=df3.apply(hitStart,axis=1)
    df3['Hit ref end']=df3.apply(hitEnd,axis=1)

    df=df3[['subject','query','LoxP ref orientation']].groupby(['subject','LoxP ref orientation'])
    return df3

def preDFs(df,loxP_type):
    #df=df[df['loxP type']==loxP_type]
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100

    df['reads_all']=df[['A','T','C','G']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base %'] = (df['top_base'] / df['reads_all'])*100
    df['top_base_accuracy']=df.apply(accuracy,axis=1,args=('top_base_seq',))
    df['mut type']=df.apply(mut_type,axis=1,args=('top_base_seq',))

    df2=df[['pos','Determinant type','majority base %', 'deletions %']]
    df2=pd.melt(df2,id_vars=['pos','Determinant type'],value_vars=['majority base %','deletions %'])
    df2=df2.rename(columns={'variable':'base type','value':'Percent of reads in pileup'})
    dfd=df2[df2['base type']=='deletions %']
    dfd=dfd.rename(columns={'Percent of reads in pileup':'Percent of deletions in reads'})
    dfm=df2[df2['base type']=='majority base %']
    dfm=dfm.rename(columns={'Percent of reads in pileup':'Percent of majority base in reads'})

    df3=df[['pos','Determinant type','top_base_accuracy']]
    df3=pd.melt(df3,id_vars=['pos','Determinant type'],value_vars=['top_base_accuracy'])
    df3=df3.rename(columns={'variable':'base type','value':'Different to reference'})

    df4=df[['pos','reads_all','Determinant type']]
    df4=df4.rename(columns={'reads_all':'Depth of coverage'})
    return df,df2,df3,df4,dfd,dfm

def setBackGrounds(r,ax):
    ax.axvspan(r['query start'],r['query end'], alpha=0.35)
    return ax

def plot(df,dfl,title,rb):
    df,df2,df3,df4,dfd,dfm=preDFs(df,'allLoxp')

    # subplots 
    nrows,ncols=3,1
    fig, ax = plt.subplots(nrows=nrows,ncols=ncols,
            gridspec_kw = {'height_ratios':[3,3,3]} )
                #'width_ratios':[5,2]})
    
    ax[2].get_shared_x_axes().join( ax[2],ax[1],ax[0] )
    # left hand plot
    g3=sns.lineplot(x='pos',y='Percent of deletions in reads',hue='Determinant type',data=dfd,ax=ax[2],
		alpha=0.5)
    g2=sns.lineplot(x='pos',y='Percent of majority base in reads',hue='Determinant type',data=dfm,ax=ax[1],
		alpha=0.5)
    g1=sns.lineplot(x='pos',y='Depth of coverage',data=df4,hue='Determinant type',ax=ax[0],
		alpha=0.5)

    # loxP sites on background
    rb=rb[rb['alignment length'] > 45]
    for index, r in rb.iterrows():
        ax[0].text(r['query start'], 
		max(df4['Depth of coverage'])+40, 
		r['subject'],
		verticalalignment='top')

        for x in range(nrows):
            ax[x].axvspan(r['query start'],r['query end'], alpha=0.35)

    plt.suptitle(title.replace('_',' '))
    fig.set_size_inches(14.5, 8.5)
    plt.legend(loc='center right')
    plt.savefig('{0}_pass_fail.pdf'.format(title))

### bam manipulations

def roundup(x,n=50):
    return int(n * round(float(x)/n))

class crisont:
    def __init__( self, blast_file, bam_file, ref_file, ref_blast_file, determinants_file, barcode ):
        self.blast_file=blast_file
        self.bam_file=bam_file
        self.title=self.bam_file.split('/')[-1].split('.')[0]
        self.ref_file=ref_file
        self.ref_blast_file=ref_blast_file
        self.determinats_file=determinants_file
        self.barcode=barcode
    
    def loadDeterminants(self):
        df=pd.read_csv(self.determinats_file)
        df=df[df['barcode']==self.barcode]
        self.determinants=df.to_dict('records')
        if len(self.determinants)>1:
            print('Too many rows per barcode')
            sys.exit()
        self.determinants = self.determinants[0]
        self.detNames=[]
        for d in range(1,self.determinants['Number of determinants']+1):
            self.detNames.append(self.determinants['name of determinant {0}'.format(d)])
        print(self.detNames)

    def binReads(self):
        '''take blast file against raw reads and groupby hit numbers and types'''
        # read blast file remove short hits and duplicate hits with near similart start positions
        df2=pd.read_csv(self.blast_file,sep='\t',names=names)

        df=df2[df2['subject'].isin(self.detNames)]
        for d in range(1,self.determinants['Number of determinants']+1):
            length=self.determinants['Length of determinant {}'.format(d)]
            cuttoff=(length/100)*self.determinants['length %']
            df=df[ df['alignment length'] > cuttoff ]
        df['fqstart']=df['query start'].map(roundup)
        idx=df.groupby(['query','fqstart'])['bit score'].transform(max) == df['bit score']
        df=df[idx]
        
        df['determinants'] = df.groupby('query')['query'].transform('count')
        df['determinants count'] = df.groupby(['query','subject'])['subject'].transform('count')
        
        # bin by number of 
        passL=df[df.determinants == self.determinants['Number of determinants']]
        passL=passL[passL['determinants count']==1]

        passed=passL['query'].tolist()
        failed=df2[~df2['query'].isin(passed)]
        failed=failed['query'].tolist()

        d={'passed':passed,
            'failed':failed}
        self.bins=d
    
    def filterBam(self,ob,l,include=True):
        '''take bam in put files, output file name and a list of seq ids not to include '''
        inBam=pysam.AlignmentFile(self.bam_file, "rb")
        outBam = pysam.AlignmentFile(ob, "wb", template=inBam)
        n=0
        for read in inBam.fetch():
            if include==True:
                if read.query_name in l:
                    n+=1
                    outBam.write(read)
            else:
                if read.query_name not in l:
                    n+=1
                    outBam.write(read)
        outBam.close()
        
        try:
            pysam.index(ob)
        except:
            print('Could not index {0} file'.format(ob))
    
        return len(l), n
    
    def _pysamStats(self,b,r):
        mybam = pysam.AlignmentFile(b)
        for rec in pysamstats.stat_variation_strand(mybam,fafile=r):
            yield rec
    
    def getPysamstats(self,b,r):
        ps=self._pysamStats(b,r)
        df=pd.DataFrame(ps)
        return df

    def processBins(self): 
        self.pysamStats=[]
        self.stats=[]
        for b in self.bins:
            l,n=self.filterBam('{0}_{1}.bam'.format(self.title,b), self.bins[b])
            d={'sample':self.title,'loxp mix':b,'reads':l,'mapped':n}
            self.stats.append(d)
            print(b,l,n)
            if n<10: continue
            # get pysam stats for filtered bam
            df=self.getPysamstats('{0}_{1}.bam'.format(self.title,b),self.ref_file)
            df['Determinant type']=b
            self.pysamStats.append(df)

        # last iter for no determinants
#        l,n=self.filterBam('{0}_noloxp.bam'.format(self.title), self.bins['allLoxp'],include=False)
#        df=self.getPysamstats('{0}_noloxp.bam'.format(self.title),self.ref_file)
#        df['loxP type']='No loxP'
#        self.pysamStats.append(df)
#        print('No loxP',l,n)
        d={'sample':self.title,'loxp mix':'No loxP','reads':l,'mapped':n}
        stats_df=pd.DataFrame(self.stats)
        stats_df.to_csv('{0}_stats.csv'.format(self.title),index=False)
        self.stats.append(d)
        self.df=pd.concat(self.pysamStats)

    
    def run(self):
        # manipulate bam file
        # determine bins
        self.loadDeterminants()
        self.binReads()
        self.processBins()
    
        # plot data
        b=loadBlast(self.blast_file)
        s=pd.DataFrame(loadBam(self.bam_file))
        dfl=mergeBamBlast(b,s)
        rb=loadBlast(self.ref_blast_file)
        plot(self.df,dfl,self.title,rb)
        
if __name__ == '__main__':
    parser = ArgumentParser(description='viz crispr regions from amplicon seq')
    parser.add_argument('-b','--blast_file', required=True,
		help='blast file of loxp or cre against reads')
    parser.add_argument('-s','--sam_file', required=True,
		help='sorted and indexed bam file of reads against ref file')
    parser.add_argument('-r','--ref_file', required=True,
		help='ref file')
    parser.add_argument('-rb','--ref_blast', required=True,
                help='blast of loxP against ref file')
    parser.add_argument('-d','--determinants_file', required=True,
                help='Determinants spreadsheet file based on template')
    parser.add_argument('-x','--barcode', required=True,
                help='The barcode used')
    
    opts, unknown_args = parser.parse_known_args()
    
    ###
    c=crisont(opts.blast_file, opts.sam_file, opts.ref_file, opts.ref_blast, opts.determinants_file, opts.barcode)
    c.run()
